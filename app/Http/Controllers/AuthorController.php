<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Author;
use App\Models\Book;

class AuthorController extends Controller
{
    public function haveBook(){
        // return authors list who have at least one book
        $author = Author::has('book')->get();

        if($author->count() > 0){
            return response()->json($author);
        }
        else {
            return response()->json("No data found");
        }
        
    }
    public function haveNoBook(){
        // return authors list who have no books
        $authorNoBook = Author::doesntHave('book')->get();

        if($authorNoBook->count() > 0){
            return response()->json($authorNoBook);
        }
        else {
            return response()->json("No data found");
        }
        
    }
    
}
