<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/hello', function () {
//     return 'Hello test route';
// });
// Route::get('/book', 'BookController@index')->name('showbook');notHaveBook
Route::get('/author', 'AuthorController@haveBook')->name('showauthor');
Route::get('/author/nobook', 'AuthorController@haveNoBook')->name('doesnothave');
