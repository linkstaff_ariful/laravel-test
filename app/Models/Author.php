<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    //fillable
    protected $fillable = [
        'first_name',
        'last_name',
    ];
    public function book(){
        
        return $this->belongsToMany('\App\Models\Book','book_authors');
    }
}
