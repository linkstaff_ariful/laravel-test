<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //fillable
    protected $fillable = [
        'book_name',
        'publish_year',
        'book_page',
    ];

    public function author()
    {
        return $this->belongsToMany('\App\Models\Author','book_authors');
    }
}
